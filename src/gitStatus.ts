import * as fs from 'fs-extra';
import path from 'path';
import utils from './gitUtils';
import Logger from './loggeer';
import colors, { Color } from 'colors';
import Table, { Cell, VerticalTableRow, CrossTableRow } from 'cli-table3';

const gitUtils = new utils();

const logger = new Logger();

const table = new Table({
    head: [
        colors.yellow('REPO'),
        colors.yellow('BRANCH'),
        colors.yellow('CHANGES')
    ]
    // colWidths: [50, 10, 50]
});

const gitList: string[] = [];

export const checkStatus = async (dir: string, branch: string) => {
    return new Promise(async resolve => {
        filewalker(dir, async (err: Error) => {
            if (err) {
                throw err;
            }
            await getStatusToTable(branch, gitList);
            logger.printToConsole(table.sort().toString());
            resolve();
        });
    });
};

async function getStatusToTable(branch: string, list: any) {
    await Promise.all(
        list.map(async (file: string) => {
            try {
                const res = (await gitUtils.gitStatus(file)) as any;

                const split = res.split('|');
                const currBranch = split[0];
                const changes = split[1];
                const repo = file.substring(file.lastIndexOf('/') + 1);

                if (branch === 'master') {
                    if (currBranch === 'master') {
                        return pushByColor(
                            currBranch,
                            repo,
                            changes,
                            colors.cyan
                        );
                    }
                    return pushByColor(
                        currBranch,
                        repo,
                        changes,
                        colors.magenta
                    );
                }
                if (currBranch !== 'master') {
                    return pushByColor(
                        currBranch,
                        repo,
                        changes,
                        colors.magenta
                    );
                }
            } catch (err) {
                return err;
            }
        })
    );
}

function pushByColor(
    currBranch: string,
    repo: string,
    changes: string,
    color: Color
) {
    if (changes.includes('nothing to commit')) {
        return table.push([repo, color(currBranch)] as Cell[] &
            VerticalTableRow &
            CrossTableRow);
    }
    if (changes.includes('Changes to be committed')) {
        return table.push([
            repo,
            color(currBranch),
            colors.green(changes)
        ] as Cell[] & VerticalTableRow & CrossTableRow);
    }
    return table.push([repo, color(currBranch), colors.red(changes)] as Cell[] &
        VerticalTableRow &
        CrossTableRow);
}

async function filewalker(dir: string, done: any) {
    let results: any = [];
    fs.readdir(dir, (err, list) => {
        if (err) return done(err);

        let pending = list.length;

        if (!pending) return done(null, gitList);
        list.forEach(async file => {
            if (file === '.git') {
                const resolvedFile = path.resolve(dir, file);

                if (!resolvedFile.includes('node_modules')) {
                    // console.log(dir);
                    gitList.push(dir);
                }
                // console.log(gitList);
                // results.push(resolvedFile);

                pending -= 1;
                if (!pending) {
                    done(null);
                }
            } else {
                const resolvedFile = path.resolve(dir, file);
                fs.stat(resolvedFile, (err2, stat) => {
                    if (err2) {
                        //
                    }
                    // If directory, execute a recursive call
                    if (stat && stat.isDirectory()) {
                        // Add directory to array [comment if you need to remove the directories from the array]
                        results.push(resolvedFile);

                        filewalker(resolvedFile, (res: any) => {
                            results = results.concat(res);
                            // gitList = gitList.concat(res);
                            pending -= 1;
                            if (!pending) {
                                done(null);
                            }
                        });
                    } else {
                        results.push(resolvedFile);
                        pending -= 1;
                        if (!pending) {
                            done(null);
                        }
                    }
                });
            }
        });
    });
}
