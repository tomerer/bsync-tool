import * as fs from 'fs-extra';

import utils from './gitUtils';

const gitUtils = new utils();

const newFileLines: any = [];

export const rollback = async (file: string) => {
    return new Promise(async resolve => {
        const content = fs.readFileSync(file, 'utf8');
        await processFile(content);
        fs.unlinkSync(file);
        newFileLines.forEach((line: string) => {
            fs.appendFileSync(file, `${line}\n`);
        });
        resolve();
    });
};

async function processFile(content: string) {
    // handle each line of stashFile
    const lines = content.split('\n');
    await Promise.all(
        lines.map(async line => {
            const split = line.split(',');
            const repo = split[0];
            const branch = split[1];

            // checkStatus. if there are uncommited changes, skip.
            const res = (await gitUtils.gitStatus(repo)) as any;
            const split2 = res.split('|');
            const changes = split2[1];
            if (changes !== 'Changes not staged for commit') {
                // checkout to branch
                await gitUtils.gitCheckout(repo, branch);
                // stash pop
                await gitUtils.gitStashPop(repo);
            } else {
                newFileLines.push(line);
            }
        })
    );

    // consider changes has been commited,deleting stashFile.txt
    // fs.unlinkSync(file);
}
