import * as fs from 'fs-extra';
import path from 'path';

import utils from './gitUtils';

const gitUtils = new utils();

export const switchBranch = async (dir: string, selectedBranch: string) => {
    return new Promise(async resolve => {
        filewalker(dir, selectedBranch, (err: Error) => {
            if (err) {
                throw err;
            }

            resolve();
        });
    });
};

async function filewalker(dir: string, selectedBranch: string, done: any) {
    let results: any = [];

    fs.readdir(dir, async (err, list) => {
        if (err) return done(err);

        let pending = list.length;

        if (!pending) return done(null, results);
        list.forEach(async file => {
            if (file === '.git') {
                const resolvedFile = path.resolve(dir, file);
                if (!resolvedFile.includes('node_modules')) {
                    try {
                        const res = (await gitUtils.gitStatus(dir)) as string;
                        const split = res.split('|');
                        const currBranch = split[0];
                        const changes = split[1];
                        // const repo = dir.substring(dir.lastIndexOf('/') + 1);
                        if (changes === 'Changes not staged for commit') {
                            await gitUtils.gitStash(dir, currBranch);
                        }
                        if (selectedBranch === 'master') {
                            await gitUtils.checkout(dir, 'master');
                        } else {
                            const found = (await gitUtils.checkIfBranchExist(
                                dir,
                                selectedBranch
                            )) as string;

                            if (found !== '') {
                                await gitUtils.checkout(dir, found);
                            } else {
                                await gitUtils.checkout(dir, 'master');
                            }
                        }
                    } catch (err) {
                        return err;
                    }
                }
                pending -= 1;
                if (!pending) {
                    done(null, results);
                }
            } else {
                const resolvedFile = path.resolve(dir, file);
                fs.stat(resolvedFile, async (err2, stat) => {
                    if (err2) {
                        //
                    }
                    // If directory, execute a recursive call
                    if (stat && stat.isDirectory()) {
                        // Add directory to array [comment if you need to remove the directories from the array]
                        results.push(resolvedFile);

                        await filewalker(
                            resolvedFile,
                            selectedBranch,
                            (res: string) => {
                                results = results.concat(res);
                                pending -= 1;
                                if (!pending) {
                                    done(null, results);
                                }
                            }
                        );
                    } else {
                        results.push(resolvedFile);
                        pending -= 1;
                        if (!pending) {
                            done(null, results);
                        }
                    }
                });
            }
        });
    });
}
