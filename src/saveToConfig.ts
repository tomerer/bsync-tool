import * as fs from 'fs-extra';
import path from 'path';
import userHome from 'user-home';

import Logger from './loggeer';
const logger = new Logger();

export const createConfigFile = () => {
    const configJson = {
        defaultPath: '',
        token: '',
        backendId: 4681008,
        frontendId: 4681109,
        localcoinId: 836618
    };
    const data = JSON.stringify(configJson);
    logger.printToConsole('create');
    fs.ensureDirSync(`${userHome}/.bsync-config`);
    fs.writeFileSync(`${userHome}/.bsync-config/config.json`, data);
};

export const saveTokenToConfigFile = (token: string) => {
    const configFile = JSON.parse(
        fs.readFileSync(`${userHome}/.bsync-config/config.json`, 'utf8')
    );
    configFile.token = token;
    const data = JSON.stringify(configFile);
    fs.writeFileSync(`${userHome}/.bsync-config/config.json`, data);
    logger.printToConsole('Token has been saved to config file.');
};
export const savePathToConfigFile = (dirPath: string) => {
    let absPath = dirPath;
    if (absPath !== '') {
        if (!path.isAbsolute(absPath)) {
            absPath = path.resolve(absPath);
        }
    }
    const configFile = JSON.parse(
        fs.readFileSync(`${userHome}/.bsync-config/config.json`, 'utf8')
    );
    configFile.defaultPath = absPath;
    const data = JSON.stringify(configFile);
    fs.writeFileSync(`${userHome}/.bsync-config/config.json`, data);
    logger.printToConsole('Path has been saved to config file.');
};

export const deleteStashFile = (file: string) => {
    if (fs.pathExistsSync(file)) {
        fs.unlinkSync(file);
        logger.printToConsole('Stash File has been deleted.');
    }
};
