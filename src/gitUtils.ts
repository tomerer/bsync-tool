// 'use strict';
import * as fs from 'fs-extra';
import { spawn } from 'child_process';

import userHome from 'user-home';

import Logger from './loggeer';

const logger = new Logger();

const config = JSON.parse(
    fs.readFileSync(`${userHome}/.bsync-config/config.json`, 'utf8')
);

import colors from 'colors';

class GitUtils {
    constructor() {
        //
    }

    // apply git stash pop
    public gitStashPop(repo: string) {
        let finish = false;
        // wrap spawn with promise
        return new Promise(resolve => {
            const checkout = spawn('git', ['stash', 'pop'], { cwd: repo });

            checkout.on('close', () => {
                // console.log(`child process exited with code ${code}`);
                finish = true;
                return resolve(finish);
            });
        });
    }

    public gitCheckout(repo: string, branch: string) {
        return new Promise(resolve => {
            const checkout = spawn('git', ['checkout', branch], { cwd: repo });

            checkout.on('close', () => {
                //  console.log(`child process exited with code ${code}`);
                return resolve();
            });
        });
    }

    public gitPull(path: string, projectName: string) {
        return new Promise(resolve => {
            const gitPull = spawn('git', ['pull'], { cwd: path });
            logger.printToConsole(colors.red(`Start updating ${projectName}`));

            gitPull.on('close', () => {
                logger.printToConsole(
                    colors.green(`Done updating ${projectName}`)
                );
                return resolve();
            });
        });
    }

    public gitClone(url: string, path: string, projectName: string) {
        return new Promise(resolve => {
            const gitPull = spawn('git', ['clone', url, path]);

            logger.printToConsole(colors.red(`Start cloning ${projectName}`));

            gitPull.on('close', () => {
                logger.printToConsole(
                    colors.green(`Done cloning ${projectName}`)
                );
                return resolve();
            });
        });
    }

    public gitStash(path: string, currBranch: string) {
        return new Promise(resolve => {
            const stash = spawn('git', ['stash', 'save', '"change"'], {
                cwd: path
            });

            stash.on('close', () => {
                // console.log(`child process exited with code ${code}`);
                fs.appendFileSync(
                    `${config.defaultPath}/stashFile.txt`,
                    `${path},${currBranch}\n`
                );
                return resolve();
            });
        });
    }

    public gitStatus(path: string) {
        return new Promise(resolve2 => {
            const status = spawn('git', ['status'], { cwd: path }) as any;
            let res = ' | ';

            status.stdout.on('data', (data: any) => {
                if (data.includes('Changes not staged for commit')) {
                    res = `${data.slice(
                        10,
                        data.indexOf('\n')
                    )}|Changes not staged for commit`;
                }
                if (data.includes('nothing to commit, working tree clean')) {
                    res = `${data.slice(
                        10,
                        data.indexOf('\n')
                    )}|nothing to commit`;
                }
                if (
                    data.includes(
                        'nothing added to commit but untracked files present'
                    )
                ) {
                    res = `${data.slice(
                        10,
                        data.indexOf('\n')
                    )}|Untracked files present`;
                }
                if (data.includes('Changes to be committed')) {
                    res = `${data.slice(
                        10,
                        data.indexOf('\n')
                    )}|Changes to be committed`;
                }
                if (data.includes('You have unmerged paths')) {
                    res = `${data.slice(
                        10,
                        data.indexOf('\n')
                    )}|You have unmerged paths`;
                }
            });

            status.on('close', () => {
                // console.log(`child process exited with code ${code}`);
                return resolve2(res);
            });
        });
    }

    public checkout(path: string, branch: string) {
        return new Promise(resolve2 => {
            const checkout = spawn('git', ['checkout', branch], { cwd: path });

            checkout.on('close', () => {
                // console.log(`child process exited with code ${code}`);
                return resolve2();
            });
        });
    }

    public checkIfBranchExist(path: string, branch: string) {
        return new Promise(resolve2 => {
            let found = false;
            let b: string;
            const grep = spawn('grep', [branch]) as any;
            const gitBranch = spawn('git', ['branch', '-r'], {
                cwd: path
            }) as any;

            gitBranch.stdout.pipe(grep.stdin);
            grep.stdout.on('data', async (data: any) => {
                found = true;
                b = data.toString('utf8');
            });

            grep.on('close', () => {
                // console.log(`child process exited with code ${code}`);
                if (found) {
                    const a = b.slice(9, b.indexOf('\n'));
                    if (a.includes('HEAD')) {
                        return resolve2('master');
                    }
                    return resolve2(b.slice(9, b.indexOf('\n')));
                }
                // console.log('close');
                return resolve2('');
            });
        });
    }
}

export default GitUtils;
