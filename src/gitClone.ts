import * as fs from 'fs-extra';
import * as gitlab from 'node-gitlab';
import userHome from 'user-home';

const config = JSON.parse(
    fs.readFileSync(`${userHome}/.bsync-config/config.json`, 'utf8')
);

import utils from './gitUtils';
import Logger from './loggeer';
const logger = new Logger();

const client = gitlab.create({
    api: 'https://gitlab.com/api/v4',
    privateToken: config.token
});

const gitUtils = new utils();

export const cloneGroups = async (id: number, dir: string) => {
    return await getGroupById(id, dir);
};

async function getGroupById(id: number, dir: string) {
    return new Promise((resolve: any, reject: any) => {
        client.groups.get({ id }, async (err: Error, group: any) => {
            if (err !== null) {
                // console.log(err);
                return reject(err);
            }

            // creating folder by group name if doesnt exists
            createFolder(group, dir);

            // clone projects to folder
            await cloneProjects(group, dir);

            try {
                await getGroupListById(id, dir);
            } catch (err) {
                logger.printToConsole('Error occurred on gitlab. Try again.');
            }

            resolve();
        });
    });
}

function createFolder(group: any, dir: string) {
    const path = group.full_path;
    const fullPath = `${dir}/${path}`;

    fs.ensureDirSync(fullPath);
}

async function cloneProjects(group: any, dir: string) {
    const projects = group.projects;
    await Promise.all(
        projects.map(async (project: any) => {
            const projectName = project.name;
            const url = project.ssh_url_to_repo;
            const path = `${dir}/${project.path_with_namespace}`;
            if (fs.pathExistsSync(path)) {
                return gitUtils.gitPull(path, projectName);
            }
            return gitUtils.gitClone(url, path, projectName);
        })
    );
}

async function getGroupListById(id: number, dir: string) {
    return new Promise((resolve: any, reject: any) => {
        client.groups.list({ id }, async (err: Error, groups: any) => {
            if (err !== null) {
                return reject(err);
            }
            if (groups.length === 0) {
                resolve(false);
            }

            for (const group of groups) {
                await getGroupById(group.id, dir);
            }

            resolve(true);
        });
    });
}
