#!/usr/bin/env node
'use strict';

import {
    saveTokenToConfigFile,
    savePathToConfigFile,
    deleteStashFile,
    createConfigFile
} from './saveToConfig';

import userHome from 'user-home';
import * as fs from 'fs-extra';

// check if config file exist, else it creates it.
if (!fs.pathExistsSync(`${userHome}/.bsync-config/config.json`)) {
    createConfigFile();
}

const configFile = JSON.parse(
    fs.readFileSync(`${userHome}/.bsync-config/config.json`, 'utf8')
);

// import { cloneGroups } from './gitClone';
import { npmInstall } from './npmInstall';
import { switchBranch } from './gitBranch';
import { checkStatus } from './gitStatus';
import { rollback } from './rollBack';

import program from 'commander';

import Logger from './loggeer';

const logger = new Logger();

program
    .command('update')
    .description(
        'Goes to gitlab and Clone/Pull everything to dir (optional: -d [path-to-dir] or default path from config file)'
    )
    .action(async () => {
        if (configFile.token === '') {
            return logger.printToConsole(
                '   Error! Token was not set!\n  Use bsync set -t <token>'
            );
        }
        if (configFile.defaultPath === '') {
            return logger.printToConsole(
                '   Error! Set default path (use bsync set -d <dir>)'
            );
        }
        try {
            const configFile2 = JSON.parse(
                fs.readFileSync(`${userHome}/.bsync-config/config.json`, 'utf8')
            );
            delete require.cache[require.resolve('./gitClone')];
            // await require('./gitClone').cloneGroups(
            //     configFile2.localcoinId,
            //     configFile2.defaultPath
            // );
            await require('./gitClone').cloneGroups(
                configFile2.backendId,
                configFile2.defaultPath
            );
            await require('./gitClone').cloneGroups(
                configFile2.frontendId,
                configFile2.defaultPath
            );
            checkStatus(`${configFile2.defaultPath}/localcoin`, 'master');
        } catch (err) {
            // console.log(err);
            if (err.name === 'Gitlab401Error') {
                logger.printToConsole(
                    'gitlab token incorrect, use bsync set -t <token>'
                );
            } else {
                logger.printToConsole('Error occurred on gitlab. Try again.');
            }
        }
    });

program
    .command('branch <branch>')
    .description('Switch to the required branch if exists or to master')
    .action(async branch => {
        const configFile2 = JSON.parse(
            fs.readFileSync(`${userHome}/.bsync-config/config.json`, 'utf8')
        );
        await switchBranch(`${configFile2.defaultPath}/localcoin`, branch);
        checkStatus(`${configFile2.defaultPath}/localcoin`, branch);
    });

program
    .command('set')
    .description('Save to configuration file')
    .option('-t, --token <token>', 'Set token')
    .option('-d, --dir <dir>', 'Set default path')
    .action(options => {
        if (options.token !== undefined) {
            saveTokenToConfigFile(options.token);
        }
        if (options.dir !== undefined) {
            savePathToConfigFile(options.dir);
        }
    });

program
    .command('rollback')
    .description('Return each service to its previous state')
    .action(async () => {
        const configFile2 = JSON.parse(
            fs.readFileSync(`${userHome}/.bsync-config/config.json`, 'utf8')
        );
        await rollback(`${configFile2.defaultPath}/stashFile.txt`);
        checkStatus(`${configFile2.defaultPath}/localcoin`, 'master');

        // maybe it should delete stashFile.txt here
    });

program
    .command('status')
    .description(
        'Show status for every git repo on which branch it is currently found, (-a, --all to show includes master)'
    )
    .option('-a, --all', 'Show all includes master', 'master')
    .action(options => {
        const configFile2 = JSON.parse(
            fs.readFileSync(`${userHome}/.bsync-config/config.json`, 'utf8')
        );
        if (options.all === 'master') {
            checkStatus(`${configFile2.defaultPath}/localcoin`, 'master');
        } else {
            checkStatus(`${configFile2.defaultPath}/localcoin`, '');
        }
    });

program
    .command('reset')
    .description('Reset configuration file to default and delete stashFile')
    .action(() => {
        createConfigFile();
        const configFile2 = JSON.parse(
            fs.readFileSync(`${userHome}/.bsync-config/config.json`, 'utf8')
        );
        deleteStashFile(`${configFile2.defaultPath}/stashFile.txt`);
    });

program
    .command('install-modules')
    .description('Execute npm install for every repo')
    .action(() => {
        npmInstall(`${configFile.defaultPath}/localcoin`);
    });

program.parse(process.argv);
