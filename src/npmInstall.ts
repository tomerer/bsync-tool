import * as fs from 'fs-extra';
import path from 'path';

import { spawn } from 'child_process';
import Logger from './loggeer';
const logger = new Logger();

export const npmInstall = async (dir: string) => {
    return new Promise(async resolve => {
        filewalker(dir, async (err: Error, gitList: any) => {
            if (err) {
                throw err;
            }
            await execInstall(gitList);
            resolve();
        });
    });
};

async function execInstall(list: any) {
    await Promise.all(
        list.map((file: string) => {
            return npmi(file);
        })
    );
}

function npmi(dir: string) {
    return new Promise(resolve => {
        const src = `${dir}/src`;
        let npm;
        if (fs.pathExistsSync(src)) {
            logger.printToConsole(src);
            npm = spawn('npm', ['i'], { cwd: src });
        } else {
            logger.printToConsole(dir);
            npm = spawn('npm', ['i'], { cwd: dir });
        }

        npm.on('close', () => {
            logger.printToConsole(`${dir} Done`);
            return resolve();
        });
    });
}

async function filewalker(dir: string, done: any) {
    let results: any = [];
    let gitList: any = [];

    fs.readdir(dir, (err, list) => {
        if (err) return done(err);

        let pending = list.length;

        if (!pending) return done(null, gitList);
        list.forEach(async file => {
            if (file === '.git') {
                const resolvedFile = path.resolve(dir, file);
                if (!resolvedFile.includes('node_modules')) {
                    gitList.push(dir);
                }
                pending -= 1;
                if (!pending) {
                    done(null, results);
                } else {
                    pending += 1;
                }
            } else {
                const resolvedFile = path.resolve(dir, file);
                fs.stat(resolvedFile, (err2, stat) => {
                    if (err2) {
                        //
                    }
                    // If directory, execute a recursive call
                    if (stat && stat.isDirectory()) {
                        // Add directory to array [comment if you need to remove the directories from the array]
                        results.push(resolvedFile);

                        filewalker(resolvedFile, (res: string) => {
                            results = results.concat(res);
                            gitList = gitList.concat(res);
                            pending -= 1;
                            if (!pending) {
                                done(null, results);
                            } else {
                                pending += 1;
                            }
                        });
                    } else {
                        results.push(resolvedFile);
                        pending -= 1;
                        if (!pending) {
                            done(null, results);
                        } else {
                            pending += 1;
                        }
                    }
                });
            }
        });
    });
}
