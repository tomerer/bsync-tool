module.exports = {
    transform: {
        '^.+\\.(t|j)s?$': 'ts-jest'
    },
    testRegex: '(/src/*|(\\.|/)(test|spec))\\.(js?|ts?)$',
    moduleFileExtensions: ['ts', 'js', 'json', 'node'],
    watchPlugins: [
        'jest-watch-typeahead/filename',
        'jest-watch-typeahead/testname'
    ]
};
