# Scarf package template

Starting point to any scarf related package

## Getting started

create a folder named as the package, use kebab-case.

```sh
mkdir [package-name]
```

clone this template into newly created folder.

```sh
cd [package-name]
git clone git@gitlab.com:scarf/scarf-package-template.git .
```

reinit git.

```sh
rm -rf .git
git init
```

edit package.json

-   replace `"name": "package-name"` with chosen package name. make sure its identical to the folder's name.
-   replace `"repository": "gitlab.com:scarf/scarf-package-template.git"` with the newly gitlab repository url.
-   fill in `"description": ""`

install dependencies

```sh
npm i
```

## Usage

`npm start` - builds the project and keep the compiler watching for changes. use in combination with npm link for seamless development.

`npm test` - starts tdd environment.

`npm run deploy` - patch the package version and publish to npm.
